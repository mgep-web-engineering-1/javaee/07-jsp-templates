<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<jsp:include page="/WEB-INF/includes/header.jsp" />
<section>
	<jsp:include page="/WEB-INF/includes/db-article.jsp" />
	<jsp:include page="/WEB-INF/includes/dbz-article.jsp" />
	<jsp:include page="/WEB-INF/includes/dbgt-article.jsp" />
</section>
<jsp:include page="/WEB-INF/includes/footer.jsp" />