window.onload = function () {
    document.querySelectorAll(".nav-item").forEach(function (navItem) {
        navItem.addEventListener("click", navClicked);
    });
}

function navClicked(event) {
    let selected_nav = event.currentTarget.id; // Get the ID of the selected element.
    let selected_article = selected_nav.replace("-nav", "-article");

    document.querySelectorAll(".db-article").forEach(function (article) { // for each dbArticle
        if (selected_article === 'all-article' || article.id === selected_article) {
            article.classList.remove("hideme");
        } else {
            article.classList.add("hideme");
        }
    });

    document.querySelectorAll(".nav-item").forEach(function (navItem) { // for each navItem
        if (navItem.id === selected_nav) {
            navItem.classList.add("current-nav");
        } else {
            navItem.classList.remove("current-nav");
        }
    });
}