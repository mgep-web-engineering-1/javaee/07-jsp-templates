<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="css/style.css" />
	<script src="js/base.js"></script>
	<title>Dragon Ball</title>
</head>

<body>
	<header>
		<h1>Dragon Ball Plots</h1>
		<nav>
			<span id="dbo-nav" class="nav-item current-nav">Dragon Ball</span>
			<span id="dbz-nav" class="nav-item">Dragon Ball Z</span>
			<span id="dbgt-nav" class="nav-item">Dragon Ball GT</span>
			<span id="all-nav" class="nav-item">All</span>
		</nav>
	</header>