# JSP Templates

## Make it work

* Clone the repository.
* Open folder with VSCode.
* Right click and Run ```src > main > java > ... > Main.java```.
* Open http://localhost:8080 in the browser.

## Objective

If we are going to have pieces of HTML/JSP code that will be repeated in many pages, we can split it into smaller parts and include them in a single page.

* We have [```regular.html```](http://localhost:8080/regular.html) file that we want to split into **header**, **content** and **footer**.
* [```index.jsp```](http://localhost:8080/) will load ```header.jsp``` and ```footer.jsp``` and will add the content in the center.
* [```index2.jsp```](http://localhost:8080/index2.jsp) is quite similar to the previous example, but it will also add each of the articles from different JSP files (```db-article.jsp```, ```dbz-article.jsp``` and ```dbgt-article.jsp```).

## Explaination

In order to include additional JSP code into a JSP file, we need to add the following code:

```jsp
<!-- index.jsp -->
...
<jsp:include page="/WEB-INF/includes/header.jsp"/>
...
```

It will add the content in that specific place.

**WARNING** We are not using any controller in this exercise, tab switching is done using CSS+JS.

### Why WEB-INF folder?

```includes``` folder could have been placed inside ```webapp``` folder. If we would have done it, putting http://localhost:8080/includes/header.jsp whould show part of an HTML. If we puth those files inside ```webapp/WEB-INF/``` folder, browsers will not be able to access them directly.

So if you want to put some files and make them accessible for the server (back-end) but not for the browsers (front-end), you could put them inside ```webapp/WEB-INF/``` folder.

## Test your understanding

* Create ```root.jsp``` file inside ```webapp``` folder
* Create ```hidden.jsp``` file inside ```webapp/WEB-INF``` folder.
* Include ```hidden.jsp``` inside ```root.jsp```. 
* Add some random content inside both files.

Try to access them from the browser:

* http://localhost:8080/root.jsp => It will show the content of ```root.jsp``` and also from ```hidden.jsp```.
* http://localhost:8080/WEB-INF/hidden.jsp => a 404 message should be shown.

## Next and before

* Before [06-server-redirect-with-session](https://gitlab.com/mgep-web-engineering-1/javaee/06-server-redirect-with-session)
* Next [08-JSTL](https://gitlab.com/mgep-web-engineering-1/javaee/08-jstl)
